import json
import os

import requests

class Ocr:
    """
    ## Optical Character Recognition Instance
    ---
    Esta classe utiliza o serviço ocr_svc para realizar operações de reconhecimento óptico de caracteres. É a instância inicial para realizar qualquer operação com os módulos.
    """

    ep: str = ""

    def __init__ (self):
        self.ep = os.environ.get("REPLAY_ADDR")

    def __request_json_post__ (self, path: str, object: dict):

        """
        ## HTTP JSON POST
        ---
        Este método é responsável por realizar requisições HTTP do tipo POST para objetos JSON.

        Ele retorna o corpo de resposta da requisição, ou uma mensagem de erro, que indica qual foi a irregularidade ocorrida ao chamar a API.
        """

        url = self.ep + path
        print("Calling: " + url)
        
        apikey = os.environ.get('REPLAY_APIKEY')
        headers = {"X-API-KEY": apikey}
        res = requests.post(url, json = object, headers = headers, verify = False)

        if res.status_code >= 400:
            raise Exception(f"HTTP ERROR: {str(res.status_code)} - {res.text}")
        if res.headers.get("Content-Type") != None and res.headers.get("Content-Type").find("json") != -1:
            return json.loads(res.text)
        else:
            return res

    def ocr (self, options: dict[str]) -> dict:
        """
        ## Optical Character Recognition
        Faz a leitura de uma parte da tela de acordo com as configurações recebidas.

        ---
        #### Parâmetros:
            - options: Dicionário com os seguintes parâmetros:
                - X: Coordenada inicial da parte da tela que se quer ler.
                - Y: Coordenada inicial da parte da tela que se quer ler.
                - H: Altura da caixa de leitura da tela.
                - W: Comprimento da caixa de leitura da tela.
                - Resizeh: Redimensionamento da altura da caixa de leitura, melhora a visualização.
                - Resizew: Redimensionamento do comprimento da caixa de leitura, melhora a visualização.
                - Sharpen: Valor de "afiação" da imagem. É um filtro para melhorar a visualização.
                - Blur: Valor de desfoque da imagem. É um filtro para melhorar a visualização.
                - Tempfile: Caminho absoluto de um diretório para salvar a screenshot tirada da caixa de leitura.
                - Src: Arquivo em que se quer fazer o reconhecimento de caracteres.
                    - "ss": Quando este valor é informado, é retirada uma screenshot da tela.
                - Gray: Insere um filtro de preto e branco na imagem, o que facilita a leitura e reduz o custo computacional.
                - Lang: Língua utilizada para fazer o reconhecimento de caracteres.
                    - "en": English (Inglês)
                    - "por": Portuguese (Português)
                - Bytes: Recebe uma imagem em bytes para realizar o reconhecimento óptico de caracteres.

        ---
        #### Retorna:
            -> Dicionário semelhante à estrutura de dados Alto (Pode ser melhor estudada em: https://en.wikipedia.org/wiki/ALTO_(XML))
        """

        raw_response = self.__request_json_post__("/ipc/ocr/", options)
        return raw_response.json()

    def strings (self, alto_string: dict):
        """
        """
        string = []
        for v in alto_string["Layout"]["Page"]["PrintSpace"]["ComposedBlock"]:
            for v1 in v["TextBlock"]:
                for v2 in v1["TextLine"]:
                    for v3 in v2["String"]:
                        string.append(v3)
        
        return string
