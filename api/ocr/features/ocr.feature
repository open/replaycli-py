Feature: The OCR API client
# ======================== OCR method ========================
    Scenario: Using the OCR standard method
        Given the OCR client
        When the OCR method is called
        Then there must be an dictionary as a return
    
# ======================== Strings method ========================
    Scenario: Using the Strings method
        Given the OCR client
        And an OCR object
        When the Strings method is called
        Then there must be a list of dictionaries with the content read and other info