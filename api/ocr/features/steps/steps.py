from behave import *
from cli import Ocr
# ========================== OCR Standard method ==========================
@given(u'the OCR client')
def step_impl(context):
    context.client = Ocr()

@when(u'the OCR method is called')
def step_impl(context):
    context.ocr_return = context.client.ocr({
     "X":        0,
     "Y":        0,
     "H":        400,
     "W":        400,
     "Resizeh":  1200,
     "Resizew":  1200,
     "Sharpen":  1,
     "Tempfile": "some_test.jpg",
     "Src":      "ss",
     "AddRects": True,
     "Gray":     True,
     "Lang":     "por"
  })

@then(u'there must be an dictionary as a return')
def step_impl(context):
    assert "Layout" in context.ocr_return, "An error ocurred during the optical character recognition process."


# ========================== OCR Strings method ==========================
@given(u'an OCR object')
def step_impl(context):
    context.ocr_return = context.client.ocr({
     "X":        0,
     "Y":        0,
     "H":        400,
     "W":        400,
     "Resizeh":  1200,
     "Resizew":  1200,
     "Sharpen":  1,
     "Tempfile": "some_test.jpg",
     "Src":      "ss",
     "AddRects": True,
     "Gray":     True,
     "Lang":     "por"
  })

@when(u'the Strings method is called')
def step_impl(context):
    context.strings_return = context.client.strings(context.ocr_return)

@then(u'there must be a list of dictionaries with the content read and other info')
def step_impl(context):
    assert "CONTENT" in context.strings_return[0], "Something went wrong with the strings method."