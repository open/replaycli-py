Feature: The Excel API client
# ======================== Excel New method ========================
    Scenario: Using the Excel New method
        Given an Excel client
        When the Excel New method is called
        Then a new Excel sheet must be created

# ======================== Excel Write method ========================
    Scenario: Using the Excel Write method
        Given an Excel client
        And an Excel sheet
        When the Excel Write method is called on this sheet to write
        Then the writing must be successful

# ======================== Excel Read method ========================
    Scenario: Using the Excel Read method
        Given an Excel client
        And an Excel sheet
        And the text "churros" text written in the A1 cell
        When the Excel Read method is called
        Then the word "churros" must be returned in the position [0][0]