from behave import *

from cli import Excel
import os

# ===================================== Excel New =====================================
@given(u'an Excel client')
def step_impl(context):
    context.path = os.getcwd()
    context.client = Excel()

@when(u'the Excel New method is called')
def step_impl(context):
    context.new_return = context.client.new(context.path+"/test.xls", "")

@then(u'a new Excel sheet must be created')
def step_impl(context):
    assert context.new_return == "ok" and "test.xls" in os.listdir(context.path), "The Excel sheet was not created as expected."


# ===================================== Excel Write =====================================
@given(u'an Excel sheet')
def step_impl(context):
    context.new_return = context.client.new(context.path+"/test.xls", "")

@when(u'the Excel Write method is called on this sheet to write')
def step_impl(context):
    context.write_return = context.client.write(context.path+"/test.xls", "Planilha1", "A1", "churros", "s")

@then(u'the writing must be successful')
def step_impl(context):
    assert context.write_return == "ok" and context.client.read(context.path+"/test.xls", "Planilha1")[0][0] == "churros", "The Write method failed unexpectedly."


# ===================================== Excel Read =====================================
@given(u'the text "churros" text written in the A1 cell')
def step_impl(context):
    context.write_return = context.client.write(context.path+"/test.xls", "Planilha1", "A1", "churros", "s")

@when(u'the Excel Read method is called')
def step_impl(context):
    context.read_return = context.client.read(context.path+"/test.xls", "Planilha1")

@then(u'the word "churros" must be returned in the position [0][0]')
def step_impl(context):
    assert context.read_return[0][0] == "churros", "The Read method did not worked as axpected."