import json
import os

import requests

class Excel:
    """
    ## Excel Client
    ---
    Esta classe utiliza o serviço excel_svc para realizar manipulações em arquivos .xls ou .xlsx.
    """

    ep: str = ""

    def __init__ (self):
        self.ep = os.environ.get("REPLAY_ADDR")

    def __request_json_post__ (self, path: str, object: dict):

        """
        ## HTTP JSON POST
        ---
        Este método é responsável por realizar requisições HTTP do tipo POST para objetos JSON.

        Ele retorna o corpo de resposta da requisição, ou uma mensagem de erro, que indica qual foi a irregularidade ocorrida ao chamar a API.
        """

        url = self.ep + path
        print("Calling: " + url)
        
        apikey = os.environ.get('REPLAY_APIKEY')
        headers = {"X-API-KEY": apikey}
        res = requests.post(url, json = object, headers = headers, verify = False)

        if res.status_code >= 400:
            raise Exception(f"HTTP ERROR: {str(res.status_code)} - {res.text}")
        if res.headers.get("Content-Type") != None and res.headers.get("Content-Type").find("json") != -1:
            return json.loads(res.text)
        else:
            return res.text

    def read (self, path: str, sheet: str):
        """
        ## Excel Read
        Faz a leitura da planilha recebida e a retorna como uma matriz.

        ---
        #### Parâmetros:
            - path: Caminho absoluto até a planilha, incluindo o nome da mesma. (Ex.: C:/MySheets/sheet.xls)
            - sheet: Nome da aba que se deseja ler da planilha passada.

        ---
        #### Retorna:
            -> Matriz (array de arrays) contendo a aba da planilha lida. Somente é feita a leitura até as linhas e colunas que possuam caracteres válidos inseridos.
        """
        object = {
            "File": path,
            "Sheet": sheet
        }
        return self.__request_json_post__("/ipc/excel/read", object)

    def write (self, path: str, sheet: str, cell: str, val: str, cell_type: str):
        """
        ## Excel Write
        Faz a escrita de um valor numa planilha especificada

        ---
        #### Parâmetros:
            - path: Caminho absoluto até a planilha, incluindo o nome da mesma. (Ex.: C:/MySheets/sheet.xls)
            - sheet: Nome da aba que se deseja ler da planilha passada.
            - cell: Célula em que se quer escrever o valor. (Ex.: A1, B10, AZ21, etc...)
            - val: Valor que se quer inserir.
            - cell_type: Tipo de formatação para a célula. Pode ser:
                - "s": Tipo string
                - "i": Tipo integer. Se quiser escrever um número ponto flutuante, utilize string.
                - "b": Tipo booleano.

        ---
        #### Retorna:
            -> "ok" em caso de sucesso.
        """
        object = {
            "File": path,
            "Sheet": sheet,
            "Cel": cell,
            "Val": val,
            "Celtype": cell_type
        }
        return self.__request_json_post__("/ipc/excel/write", object)

    def new (self, path: str, sheet: str):
        """
        ## Excel New Sheet
        Faz a criação de uma nova planilha no caminho especificado.

        ---
        #### Parâmetros:
            - path: Caminho absoluto no qual será criada a planilha, incluindo o nome da mesma. (Ex.: C:/MySheets/newSheets/sheet.xlsx)
            - sheet: Nome que se deseja dar à segunda aba da planilha, sendo a primeira, por padrão: "Sheet1" ou "Planilha1".
            
        ---
        #### Retorna:
            -> "ok" em caso de sucesso.
        """
        object = {
            "File": path,
            "Sheet": sheet
        }
        return self.__request_json_post__("/ipc/excel/new", object)
