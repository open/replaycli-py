from re import A
from behave import *
from cli import API_Proxy

# =========================== JSON RESPONSE BODY ===========================
@given(u'a API Proxy client')
def step_impl(context):
    context.client = API_Proxy ()

@given(u'an APIProxyRequest instance filled with the method GET for the API url: https://pokeapi.co/api/v2/pokemon/ditto')
def step_impl(context):
    context.request = context.client.APIProxyRequest
    context.request["Method"] = "GET"
    context.request["Url"] = "https://pokeapi.co/api/v2/pokemon/ditto"

@when(u'a request is made with the previous instance')
def step_impl(context):
    context.response = context.client.do (context.request)

@then(u'the body of this response must be a python dictionary')
def step_impl(context):
    assert type (context.response["body"]) == dict, "Something went wrong calling the pokemon/ditto API."

@then(u'the pokemon name, contained in the response body, must be ditto')
def step_impl(context):
    assert context.response["body"]["forms"][0]["name"] == "ditto", "This pokemon is not Ditto."



# =========================== TEXT RESPONSE BODY ===========================
@given(u'an APIProxyRequest instance filled with the method GET for the API url: https://www.slashdot.org')
def step_impl(context):
    context.request = context.client.APIProxyRequest
    context.request["Method"] = "GET"
    context.request["Url"] = "https://www.slashdot.org"

@then(u'the response body content type must be string')
def step_impl(context):
    assert type (context.response["body"]) == str, "Something went wrong calling the slashdot API."