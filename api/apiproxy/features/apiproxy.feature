Feature: The API Proxy client
    Scenario: A HTTP request with json body response
        Given a API Proxy client
        And an APIProxyRequest instance filled with the method GET for the API url: https://pokeapi.co/api/v2/pokemon/ditto
        When a request is made with the previous instance
        Then the body of this response must be a python dictionary
        And the pokemon name, contained in the response body, must be ditto
    

    Scenario: A HTTP request with text body response
        Given a API Proxy client
        And an APIProxyRequest instance filled with the method GET for the API url: https://www.slashdot.org
        When a request is made with the previous instance
        Then the response body content type must be string