import base64
import json
import os
import requests

class API_Proxy:
    """
    ## API Proxy
    ---
    O Serviço API Proxy permite realizar chamadas a api externas proxiadas pelo Replay. Dessa forma, as chamadas podem ficar pré- configuradas dentro do Replay.

    Caso o robô ou aplicação desejem alterar algum parâmetro antes da chamada ainda é possível, para isso, deve-se modificar a estrutura da requisição.

    Para entender melhor a como funciona a estrutura de requisição, você pode visitar a documentaçao oficial ou dar uma olhada no método "do ()" deste client.       
    """

    ep: str = ""

    APIProxyRequest = {
        "Name": None,
        "Method": None,
        "Url": None,
        "Header": None,
        "Body": None,
        "Readonly": None
    }

    APIProxyResponse = {
        "Status": None,
        "StatusCode": None,
        "Header": None,
        "Body": None
    }

    def __init__ (self):
        self.ep = os.environ.get("REPLAY_ADDR")
    
    def __request_json_post__(self, path: str, object: dict):

        """
        ## HTTP JSON POST
        ---
        Este método é responsável por realizar requisições HTTP do tipo POST para objetos JSON.

        Ele retorna o corpo de resposta da requisição, ou uma mensagem de erro, que indica qual foi a irregularidade ocorrida ao chamar a API.
        """

        url = self.ep + path
        print("Calling: " + url)
        
        apikey = os.environ.get('REPLAY_APIKEY')
        headers = {"X-API-KEY": apikey}
        res = requests.post(url, json = object, headers = headers, verify = False)

        if res.status_code >= 400:
            raise Exception(f"HTTP ERROR: {str(res.status_code)} - {res.text}")

        
        return json.loads(res.text)

    
    def do (self, request: dict):
        """
        ## API Proxy Do
        Faz o proxy da chamada remota e retorna qualquer que seja o resultado desta requisição.

        ---
        #### Parâmetros:
            - request: Estrutura APIProxyRequest definida nesta mesma classe. Instancie-a e altere os valores das chaves de acordo com sua necessidade, veja abaixo como:
            
            {
                "Name": "Nome de template da chamada",

                "Method": "Método de requisição (GET, POST, PUT, ...)",

                "Url": "Url da API que se deseja chamar",

                "Header": {
                        "Chave da configuração de cabeçalho": "Valor associado",
                        
                        ...
                    },

                "Body": "Elementos pertencentes ao corpo da requisição",

                "Readonly": 
            
                    - True: Mesmo que seja encontrado um template correspondente ao valor de "Name" passado, as configurações desta estrutura prevalecerão na requisição.

                    - False: Caso um template seja encontrado em correspondência ao valor passado em "Name", as configurações do mesmo sobrescreverão as informações passadas nesta estrutura.
            }
        
        ---
        #### Retorna:
            -> Estrutura APIProxyResponse já definida nesta classe.
        """

        do_response = self.__request_json_post__ ("/ipc/apiproxy/do", request)
            
        do_response["body"] = base64.b64decode(do_response["body"])
        do_response["body"] = str(do_response["body"], "utf-8")

        if do_response["header"]["Content-Type"].find("json") != -1:
            do_response["body"] = json.loads(do_response["body"])
        
        return do_response
