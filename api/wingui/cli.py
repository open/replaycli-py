# import datetime
import json
import urllib
import os
from typing import List

import requests

class Wingui:
    """
    Classe que representa um proxy para um robo remoto.
    mapeia o robo através da propriedade rid
    ep significa endpoint, e aponta para o endereco na web on se hostea o serviço
    Por padrão é: https://localhost:8443
    """

    ep: str = ""

    def __init__(self):
        self.ep = os.environ.get("REPLAY_ADDR")

    def __requestget__(self, data: str):

        """
        Metodo que realiza requisições HTTP do tipo GET.
        """

        url = self.ep + data
        print("Calling: " + url)
        
        apikey = os.environ.get('REPLAY_APIKEY')
        headers = {"X-API-KEY": apikey}
        res = requests.get(url, headers=headers, verify=False)

        if res.status_code >= 400:
            raise Exception(f"HTTP ERROR: {str(res.status_code)} - {res.text}")
        if res.headers.get("Content-Type") != None and res.headers.get("Content-Type").find("json") != -1:
            return json.loads(res.text)
        else:
            return res.text
    
    def __requestpost__(self, data: str, object: dict):

        """
        Metodo que realiza requisições HTTP do tipo POST.
        """

        url = self.ep + data
        print("Calling: " + url)
        
        apikey = os.environ.get('REPLAY_APIKEY')
        headers = {"X-API-KEY": apikey}
        res = requests.post(url, json = object, headers = headers, verify = False)

        if res.status_code >= 400:
            raise Exception(f"HTTP ERROR: {str(res.status_code)} - {res.text}")
        if res.headers.get("Content-Type") != None and res.headers.get("Content-Type").find("json") != -1:
            return json.loads(res.text)
        else:
            return res.text

# Funções Clip
    def clip_read(self) -> str:
        """
        ## Clip Read
        Le o conteudo da clipboard na maquina do robo, e retorna como string

        ---
        #### Parâmetros:
            ---
        
        ---
        #### Retorna:
            -> Conteúdo da ClipBoard.
        """
        return self.__requestget__("/ipc/wingui/clip/read")

    def clip_write(self, m: str) -> str:
        """
        ## Clip Write
        Grava um texto na ClipBoard da máquina.

        ---
        #### Parâmetros:
            - m: Texto que se deseja inserir na ClipBoard
        ---
        #### Retorna: 

            ---
        """
        str = urllib.parse.quote(m, "")
        return self.__requestget__(f"/ipc/wingui/clip/write?str={str}")

# Funções Screen
    def screen_click(self, img: str):

        """
        ## Screen Click
        Realiza um click na tela, desde que encontre uma correspondência à imagem fornecida.

        ---
        #### Parâmetros:
            - img: Caminho absoluto para a imagem a ser clicada na tela.

        ---
        #### Retorna:
            -> Array contendo as coordenadas iniciais da imagem localizada na tela, as dimensões de comprimento e altura, assim como o caminho fornecido ao parâmetro img.
        """

        img = urllib.parse.quote(img, "")
        return self.__requestget__(f"/ipc/wingui/screen/click?f={img}")

    def screen_clickcenter(self, img: str):

        """
        ## Screen Click Center
        Semelhante a screen_click, porém realiza o clique no centro da imagem localizada.

        ---
        #### Parâmetros:
            - img: Caminho absoluto para a imagem a ser clicada na tela.
            
        ---
        #### Retorna:
            -> Array contendo as coordenadas iniciais da imagem localizada na tela, as dimensões de comprimento e altura, assim como o caminho fornecido ao parâmetro img.
        """

        img = urllib.parse.quote(img, "")
        return self.__requestget__(f"/ipc/wingui/screen/clickcenter?f={img}")

    def screen_capture(self, f: str, x: int, y: int, w: int, h: int):
        """
        ## Screen Capture
        Realiza uma captura de tela e salva a imagem no diretório que abriga o serviço do winGUI.

        ---
        #### Parâmetros:
            - f: Nome do arquivo a ser salvo
            - x: X da origem da imagem - coordenada mais à esquerda do retangulo
            - y: Y da origem da imagem - coordenada mais acima do retangulo
            - w: Comprimento da imagem
            - h: altura da imagem.
        ---
        #### Retorna:
            
            ---

        ---
        #### Exemplo:
            Para capturar um retangulo, do Pixel (10,10) ate o Pixel (110,100), utiliza-se:

            screen_capture("c:\\arq.png",10,10,100,100)
        """

        f = urllib.parse.quote(f, "")
        return self.__requestget__(f"/ipc/wingui/screen/capture?x={x}&y={y}&w={w}&h={h}&f={f}")

    def screen_find(self, f: str):

        """
        ## Screen Find
        Busca a imagem cujo caminho foi fornecido na tela.

        ---
        #### Parâmetros:
            - f: Caminho absoluto da imagem que se deseja buscar.

        ---
        #### Retorna:
            -> Array contendo as coordenadas iniciais da imagem localizada na tela, as dimensões de comprimento e altura, assim como o caminho fornecido ao parâmetro img.
        """

        f = urllib.parse.quote(f, "")
        return self.__requestget__(f"/ipc/wingui/screen/find?f={f}")

    def screen_findall (self, f: str):

        """
        ## Screen Find All
        Busca todas as ocorrências da imagem fornecida como parâmetro na tela.

        ---
        #### Parâmetros:
            - f: Caminho absoluto da imagem que se deseja buscar.

        ---
        #### Retorna:
            -> Array de todas as ocorrências da imagem na tela. As ocorrências são representadas por dicionários cujas chaves são o eixo da coordenada e o valor é o valor do mesmo.
        """

        f = urllib.parse.quote(f, "")
        return self.__requestget__(f"/ipc/wingui/screen/findall?f={f}")

    def screen_wait(self, f: str, m: int = 30):

        """
        ## Screen Wait
        Procura pela imagem fornecida na tela por determinado tempo.

        ---
        #### Parâmetros:
            - f: Caminho da imagem que se está buscando.
            - m: Tempo máximo pelo qual se buscará a imagem na tela.

        ---
        #### Retorna:
            -> Array contendo as coordenadas iniciais da imagem localizada na tela, as dimensões de comprimento e altura, assim como o caminho fornecido ao parâmetro img.
        """

        f = urllib.parse.quote(f, "")
        return self.__requestget__(f"/ipc/wingui/screen/wait?f={f}&m={m}")

    def screen_waitclick(self, f: str, m: int = 30):
        """
        ## Screen WaitClick
        Análogo a screen_wait, mas realiza click na coordenada x = 0, y = 0 da imagem.

        ---
        #### Parâmetros:
            - f: Caminho da imagem que se está buscando.
            - m: Tempo máximo pelo qual se buscará a imagem na tela.

        ---
        #### Retorna:
            -> Array contendo as coordenadas iniciais da imagem localizada na tela, as dimensões de comprimento e altura, assim como o caminho fornecido ao parâmetro img.
        """

        f = urllib.parse.quote(f, "")
        return self.__requestget__(f"/ipc/wingui/screen/waitclick?f={f}&m={m}")

    def screen_waitclickcenter(self, f: str, m: int = 30):
        """
        ## Screen WaitClick
        Análogo a screen_waitclick, mas realiza click nas coordenadas centrais da imagem fornecida.

        ---
        #### Parâmetros:
            - f: Caminho da imagem que se está buscando.
            - m: Tempo máximo pelo qual se buscará a imagem na tela.
            
        ---
        #### Retorna:
            -> Array contendo as coordenadas iniciais da imagem localizada na tela, as dimensões de comprimento e altura, assim como o caminho fornecido ao parâmetro img.
        """

        f = urllib.parse.quote(f, "")
        return self.__requestget__(f"/ipc/wingui/screen/waitclickcenter?f={f}&m={m}")

    def screen_scale(self):
        """
        ## Screen Scale
        Busca nas configurações de vídeo do sistema a escala do monitor.

        ---
        #### Parâmetros:

            ---

        ---
        #### Retorna:
            -> Array contendo a escala do monitor.
        """
        return self.__requestget__("/ipc/wingui/screen/scale")

    def screen_size(self):

        """
        ## Screen Scale
        Busca nas configurações de vídeo do sistema o tamanho da tela do monitor.

        ---
        #### Parâmetros:

            ---

        ---
        #### Retorna:
            -> Array contendo as dimensões da tela.
        """
        return self.__requestget__("/ipc/wingui/screen/size")

    # def screen_res(self, d: int) -> List[int]:

    #     """
    #     Retorna a resolução da tela, em um array [x,y] 
    #     """
    #     return self.__requestget__(f"/ipc/wingui/screen/res?d={d}")

    # def screen_setres(self, x: int, y: int, c: int):
    #     """
    #     Set resolução e cor da tela [DESCONTINUADA. VER display_setres]
    #     """
    #     return self.__requestget__(f"/ipc/wingui/screen/setres?x={x}&y={y}&c={c}")

    def screen_shot(self, fmt: str = "b64"):

        """
        ## Screen shot
        Faz uma captura completa da tela.

        ---
        #### Parâmetros:
            - fmt: Formato de codificação da imagem capturada. Por padrão está codificada em Base64.

        ---
        #### Retorna:
            -> Captura da tela completa exibida no monitor codificada.
        """
        return self.__requestget__(f"/ipc/wingui/screen/shot?fmt={fmt}")

    def screen_dsinternal(self):
        """
        ## Screen Display Internal
        Faz com que o monitor definido como primário (através das configurações do windows) seja o único ativo.

        ---
        #### Parâmetros:

            ---

        ---
        #### Retorna:

            ---
        """
        return self.__requestget__(f"/ipc/wingui/screen/dsinternal")

    def screen_dsexternal(self):
        """
        ## Screen Display External
        Faz com que o monitor definido como secundário (através das configurações do windows) seja o único ativo.
        
        ---
        #### Parâmetros:

            ---

        ---
        #### Retorna:

            ---
        """
        return self.__requestget__(f"/ipc/wingui/screen/dsexternal")

    def screen_dsclone(self):
        """
        ## Screen Display Clone
        Faz com que o vídeo exibido no monitor definido como primário seja clonado para o secundário.
        
        ---
        #### Parâmetros:

            ---

        ---
        #### Retorna:

            ---
        """
        return self.__requestget__(f"/ipc/wingui/screen/dsclone")
    
    def screen_dsextend(self):
        """
        ## Screen Display Extend
        Faz com que o vídeo de ambos os monitores seja independente.
        
        ---
        #### Parâmetros:

            ---

        ---
        #### Retorna:

            ---
        """
        return self.__requestget__(f"/ipc/wingui/screen/dsextend")

# Funções Draw
    def draw_addbox(self, x: int, y: int, w: int = 100, h: int = 100, t: int = 3):
        """
        ## Draw Box
        Adiciona uma caixa à tela de overlay
        
        ---
        #### Parâmetros:
            -x: Ponto inicial do eixo x para o desenho.
            -y: Ponto inicial do eixo y para o desenho.
            -w: Largura da caixa a ser desenhada.
            -h: Altura da caixa a ser desenhada.
            -t: Largura do pincel que desenhará a caixa.

            Todos os parâmetros devem ser dados em píxels.

        ---
        #### Retorna:

            ---
        """
        return self.__requestget__(f"/ipc/wingui/draw/addbox?x={x}&y={y}&w={w}&h={h}&t={t}")

    def draw_addrect(self, x: int, y: int, w: int = 100, h: int = 100):
        """
        ## Draw Rectangle
        Adiciona um retângulo opaco à tela de overlay
        
        ---
        #### Parâmetros:
            -x: Ponto inicial do eixo x para o desenho.
            -y: Ponto inicial do eixo y para o desenho.
            -w: Largura do retângulo a ser desenhado.
            -h: Altura do retângulo a ser desenhado.

            Todos os parâmetros devem ser dados em píxels.
            
        ---
        #### Retorna:

            ---
        """
        return self.__requestget__(f"/ipc/wingui/draw/addrect?x={x}&y={y}&w={w}&h={h}")

    def draw_addtext(self, x: int, y: int, t: str = "No text set"):
        """
        ## Draw Text
        Adiciona um texto à tela de overlay

        ---
        #### Parâmetros:
            -x: Ponto inicial do eixo x para a escrita.
            -y: Ponto inicial do eixo y para a escrita.
            -t: Texto a ser escrito.

        ---
        #### Retorna:

            ---
        """
        t = urllib.parse.quote(t, "")
        return self.__requestget__(f"/ipc/wingui/draw/addtext?x={x}&y={y}&t={t}")

    def draw_clear(self):
        """
        ## Draw Clear
        Limpa tela de overlay
        
        ---
        #### Parâmetros:

            ---

        ---
        #### Retorna:

            ---
        """
        return self.__requestget__("/ipc/wingui/draw/clear")

    # def draw_show(self):
    #     """mostra tela de overlay"""
    #     return self.__requestget__("/ipc/wingui/draw/show")

# Funções KB
    def kb_tap(self, s: str) -> str:
        """
        ## KB Tap
        Envia uma tecla ou teclas para serem clicadas no componente com foco onde o robo é executado. Em caso de teclas alteradoras, elas devem ser sempre posicionadas após a tecla alvo sem espaço entre a vírgula e o texto.

        ---
        #### Parâmetros:
            - s: Sequência de teclas que se deve apertar.

        ---
        #### Retorna:
            
            ---

        ---
        #### Exemplo:
            Para enviar ALT+d, você envia o string "d,alt". Para enviar C maiusculo, você utiiliza "c,shift".
        """
        str = urllib.parse.quote(s, "")
        return self.__requestget__(f'/ipc/wingui/kb/tap?str={str}')

    def kb_type(self, s: str) -> str:
        """
        ## KB Type
        Semelhante a tap, mas envia textos completos.
        
        ---
        #### Parâmetros:
            - s: Texto que se quer digitar.

        ---
        #### Retorna:
            
            ---
        """

        str = urllib.parse.quote(s, "")
        return self.__requestget__(f"/ipc/wingui/kb/type?str={str}")

    def kb_toggle(self, s: str) -> str:
        """
        ## KB Toggle
        Semelhante a tap, porém mantem a tecla ou o conjunto de teclas pressionado até que elas sejam pressionadas novamente manualmente ou pelo robô. Pode-se utilizar o método kb_tap para interromper o pressionamento por exemplo.
        
        ---
        #### Parâmetros:
            - s: Tecla ou sequência de teclas que se deseja pressionar.

        ---
        #### Retorna:
            
            ---
        """

        str = urllib.parse.quote(s, "")
        return self.__requestget__(f"/ipc/wingui/kb/toggle?str={str}")

# Funções Dialog
    def dialog_color(self, t: str = "Selecione Cor", d: str = "BEBEBE") -> bool:
        """
        ## Dialog Color
        Apresenta uma janela de diálogo para que o usuário selecione uma cor.

        ---
        #### Parâmetros:
            - t: Titulo
            - d: Cor padrao
        
        ---
        #### Retorna:
            -> Dicionário cujas chaves são "R", "G", "B" e "A" e os valores são os valores RGBA específicos da cor para cada uma das chaves.
        """

        t = urllib.parse.quote(t, "")
        d = urllib.parse.quote(d, "")
        return self.__requestget__(f'/ipc/wingui/dialog/color?t={t}&d={d}')

    def dialog_date(self, t: str = "Selecione Data", m: str = "", d="", f="") -> str:
        """
        ## Dialog Date
        Apresenta uma janela de diálogo para que o usuário selecione uma data.

        ---
        #### Parâmetros:
            - t: Título
            - m: Mensagem de texto
            - d: Data Padrao (se vazio = hoje)
            - f: formato para parse

        ---
        #### Retorna:
            -> String contendo a data selecionada. Ex: 2021-12-15T00:00:00Z
        """

        f = urllib.parse.quote(f, "")
        d = urllib.parse.quote(d, "")
        m = urllib.parse.quote(m, "")
        t = urllib.parse.quote(t, "")
        return self.__requestget__(f'/ipc/wingui/dialog/date?t={t}&m={m}&d={d}&f={f}')

    def dialog_entry(self, t: str = "Favor inserir", m: str = "Valor", d: str = "Valor") -> str:
        """
        ## Dialog Entry
        Apresenta uma janela de diálogo para entrada de um texto curto.

        ---
        #### Parâmetros:
            - t: Titulo
            - m: Mensagem de texto
            - d: valor padrao

        ---
        #### Retorna:
            -> String digitada.
        """

        t = urllib.parse.quote(t, "")
        m = urllib.parse.quote(m, "")
        d = urllib.parse.quote(d, "")
        return self.__requestget__(f'/ipc/wingui/dialog/entry?t={t}&m={m}&d={d}')

    def dialog_error(self, t: str = "Erro", m: str = "Houve um erro") -> bool:
        """
        ## Dialog Error
        Apresenta uma janela de dialog exibindo um erro definido pelo programador.

        ---
        #### Parâmetros:
            - t: Titulo
            - m: Mensagem de texto

        ---
        #### Retorna:
            ->   "ok"
        """

        t = urllib.parse.quote(t, "")
        m = urllib.parse.quote(m, "")
        return self.__requestget__(f'/ipc/wingui/dialog/error?t={t}&m={m}')

    def dialog_file(self, t: str = "Selecionar arquivo", f: str = ".", d: str = "false") -> str:
        """
        ## Dialog File
        Apresenta uma janela de seleção de arquivos ou pastas.

        ---
        #### Parâmetros:
            - t: Titulo da janela.
            - f: Diretório inicial ("." por padrão)
            - d: true -> Seleciona diretórios; false -> Seleciona arquivos

        ---
        #### Retorna:
            -> Caminho absoluto do arquivo selecionado
        """

        t = urllib.parse.quote(t, "")
        f = urllib.parse.quote(f, "")
        d = urllib.parse.quote(d, "")
        return self.__requestget__(f'/ipc/wingui/dialog/file?t={t}&f={f}&d={d}')

    def dialog_file_multi(self, t: str = "Selecionar arquivos", f: str = ".") -> List[str]:
        """
        ## Dialog File Multi
        Apresenta uma janela de seleção de múltiplos arquivos.

        ---
        #### Parâmetros:
            - t: Titulo da janela.
            - f: Diretório inicial ("." por padrão)

        ---
        #### Retorna:
            -> Lista com os caminhos absolutos dos arquivos selecionados.
        """

        t = urllib.parse.quote(t, "")
        f = urllib.parse.quote(f, "")
        return self.__requestget__(f'/ipc/wingui/dialog/filemulti?t={t}&f={f}')

    def dialog_info(self, t: str = "Info", m: str = "Info Msg") -> bool:
        """
        ## Dialog Info
        Apresenta uma janela de diálogo exibindo uma informação definida pelo programador.

        ---
        #### Parâmetros:
            - t: Titulo da janela
            - m: Mensagem exibida

        ---
        #### Retorna:
            -> "ok"
        """

        t = urllib.parse.quote(t, "")
        m = urllib.parse.quote(m, "")
        return self.__requestget__(f'/ipc/wingui/dialog/info?t={t}&m={m}')

    def dialog_list(self, t: str = "Selecionar", m: str = "Selecione uma entrada", f: List[str] = []) -> str:
        """
        ## Dialog List
        Apresenta uma janela de diálogo com uma lista. Permite o usuário selecionar um dos itens.

        ---
        #### Parâmetros:
            - t: Titulo da janela
            - m: Mensagem da janela
            - f: Opções para escolher

        ---
        #### Retorna:
            -> Opção escolhida
        """

        t = urllib.parse.quote(t, "")
        m = urllib.parse.quote(m, "")
        f = urllib.parse.quote(",".join(f), "")
        return self.__requestget__(f'/ipc/wingui/dialog/list?t={t}&m={m}&f={f}')

    def dialog_listmulti(self, t: str = "Selecionar", m: str = "Selecione uma entrada", f: List[str] = []) -> List[str]:
        """
        ## Dialog List Multi
        Apresenta uma janela de diálogo. Permite o usuário selecionar todos os itens que quiser.

        ---
        #### Parâmetros:
            - t: Titulo da janela
            - m: Mensagem da janela
            - f: Opções para escolher

        ---
        #### Retorna:
            -> Lista com as opções escolhidas
        """

        t = urllib.parse.quote(t, "")
        m = urllib.parse.quote(m, "")
        f = urllib.parse.quote(",".join(f), "")
        return self.__requestget__(f'/ipc/wingui/dialog/listmulti?t={t}&m={m}&f={f}')

    def dialog_password(self, t: str = "Entrar com Senha", m: str = "Favor fornecer sua senha") -> str:
        """
        ## Dialog Password
        Apresenta uma janela de diálogo para entrada de senha/informações confidenciais.

        ---
        #### Parâmetros:
            - t: Titulo da janela
            - m: Mensagem de texto exibida

        ---
        #### Retorna:
            -> Texto digitado.
        """

        t = urllib.parse.quote(t, "")
        m = urllib.parse.quote(m, "")
        return self.__requestget__(f'/ipc/wingui/dialog/password?t={t}&m={m}')

    def dialog_question(self, t: str = "Question", m: str = "Question Msg") -> bool:
        """
        ## Dialog Question
        Apresenta uma janela de diálogo com uma pergunta, que apresenta as opções sim/não

        ---
        #### Parâmetros:
            - t: Titulo da janela
            - m: Mensagem exibida.

        ---
        #### Retorna:
            - True: Caso clique em sim
            - False: Caso clique em não
        """

        t = urllib.parse.quote(t, "")
        m = urllib.parse.quote(m, "")
        return self.__requestget__(f'/ipc/wingui/dialog/question?t={t}&m={m}')

    def dialog_warn(self, t: str = "Warn", m: str = "Warn") -> bool:
        """
        ## Dialog Warn
        Apresenta uma janela de diálogo exibindo uma mensagem de aviso.

        ---
        #### Parâmetros:
            - t: Titulo da janela
            - m: Mensagem exibida

        ---
        #### Retorna:
            -> "ok"
        """

        t = urllib.parse.quote(t, "")
        m = urllib.parse.quote(m, "")
        return self.__requestget__(f'/ipc/wingui/dialog/warn?t={t}&m={m}')

    # def dialog_yesno(self, t: str, m: str) -> bool:
    #     """
    #     Apresenta dialogo para que usuario escolha sim ou nao
    #     Params:
    #         - t: Titulo
    #         - m: Mensagem
    #     Retorna:
    #         bool
    #     """
    #     return self.__requestget__("dialog/yesno?t={}&m={}".format(urllib.parse.quote(t, ""), urllib.parse.quote(m, "")))
    #
    # def dialog_filesave(self, t: str, d: str, x: List[str]) -> str:
    #     """
    #     Apresenta dialogo usuario arquivo para salvar arquivo
    #     Params:
    #         - t: Titulo
    #         - d: descricao
    #         - x: lista de extensoes SEM PONTO - ex: ["pdf","xls","py"]
    #     Retorna:
    #         str: Nome do arquivo
    #     """
    #     xstr = ",".join(x)
    #     return self.__requestget__(
    #         "dialog/filesave?t={}&d={}&x={}".format(urllib.parse.quote(t, ""), urllib.parse.quote(d, ""),
    #                                                 urllib.parse.quote(xstr, "")))
    #
    # def dialog_fileload(self, t: str, d: str, x: List[str]) -> str:
    #     """
    #     Apresenta dialogo usuario arquivo para carregar arquivo
    #     Params:
    #         - t: Titulo
    #         - d: descricao
    #         - x: lista de extensoes SEM PONTO - ex: ["pdf","xls","py"]
    #     Retorna:
    #         str: Nome do arquivo
    #     """
    #     xstr = ",".join(x)
    #     return self.__requestget__(
    #         "dialog/fileload?t={}&d={}&x={}".format(urllib.parse.quote(t, ""), urllib.parse.quote(d, ""),
    #                                                 urllib.parse.quote(xstr, "")))
    #
    # def dialog_dir(self, t: str) -> str:
    #     """"
    #     Apresenta dialogo usuario selecionar pasta
    #     Params:
    #         - t: Titulo
    #     Retorna:
    #         str: Nome do diretorio
    #     """
    #     return self.__requestget__("dialog/dir?t={}".format(urllib.parse.quote(t, "")))

# Funções Mouse
    def mouse_move(self, x: int, y: int) -> str:
        """
        ## Mouse Move
        Move o mouse para a coordenada X, Y

        ---
        #### Parâmetros:
            - x: Posição absoluta da tela no eixo x
            - y: Posição absoluta da tela no eixo y

        ---
        #### Retorna:

            ---
        """
        return self.__requestget__(f"/ipc/wingui/mouse/move?x={x}&y={y}")

    def mouse_drag(self, x: int, y: int, btn: str = "left") -> str:
        """
        ## Mouse Drag
        Move o mouse para a coordenada X, Y

        ---
        #### Parâmetros:
            - x: Coordenada final do eixo x
            - y: Coordenada final do eixo y

        ---
        #### Retorna:

            ---
        """

        btn = urllib.parse.quote(btn, "")
        return self.__requestget__(f'/ipc/wingui/mouse/drag?x={x}&y={y}&btn={btn}')

    def mouse_click(self) -> str:
        """
        ## Mouse Click
        Clica no local onde o mouse está repousado

        ---
        #### Parâmetros:
            ---

        ---
        #### Retorna:
            ---
        """
        return self.__requestget__("/ipc/wingui/mouse/click")

    def mouse_moverelative(self, x: int, y: int) -> str:
        """
        ## Mouse Move Relative
        Move o mouse para a coordenada X, Y, relativo a coordenada atual

        ---
        #### Parâmetros:
            - x: Quantidade de píxeis que o mouse será movido no eixo x
            - y: Quantidade de píxeis que o mouse será movido no eixo y
        ##### No caso de valores negativos, moverá relativamente o mouse para a esquerda (x) e/ou para cima (y)

        ---
        #### Retorna:
            
            ---
        """

        return self.__requestget__(f"/ipc/wingui/mouse/moverelative?x={x}&y={y}")

    def mouse_clickat(self, x: int, y: int):
        """
        ## Mouse Click At
        Move o mouse e clica em uma única operação

        ---
        #### Parâmetros:
            - x: Coordenada absoluta do eixo x no qual o robô fará o clique
            - y: Coordenada absoluta do eixo y no qual o robô fará o clique
        ---
        #### Retorna:

            ---
        """
        return self.__requestget__(f"/ipc/wingui/mouse/clickat?x={x}&y={y}")

    def mouse_clickatrelative(self, x: int, y: int):
        """
        ## Mouse Click At
        Move o mouse e clica em uma única operação

        ---
        #### Parâmetros:
            - x: Quantidade em píxels que o mouse se moverá no eixo X a partir da posição atual
            - y: Quantidade em píxels que o mouse se moverá no eixo Y a partir da posição atual

        ---
        #### Retorna:

            ---
        """

        return self.__requestget__(f"/ipc/wingui/mouse/clickatrelative?x={x}&y={y}")

# Funções Notify
    def notify_alert(self, t: str, m: str):

        """
        ## Notify Alert
        Cria um alerta do windows.

        ---
        #### Parâmetros:
            - t: Título da notificação.
            - m: Mensagem exibida pela notificação.

        ---
        #### Retorna:

            ---
        """

        t = urllib.parse.quote(t, "")
        m = urllib.parse.quote(m, "")
        return self.__requestget__(f"/ipc/wingui/notify/alert?msg={m}&title={t}")

    def notify_beep(self, t: str, m: str):

        """
        ## Notify Alert
        Cria um alerta do windows.

        ---
        #### Parâmetros:
            - t: Título da notificação.
            - m: Mensagem exibida pela notificação.

        ---
        #### Retorna:
            
            ---
        """

        t = urllib.parse.quote(t, "")
        m = urllib.parse.quote(m, "")
        return self.__requestget__(f"/ipc/wingui/notify/notify?msg={m}&title={t}")

# Funções proc
    def proc_exec(self, cmd: str):

        """
        ## Process execute
        Executa um comando cmd localmente na máquina. Para abertura de arquivos, é necessário informar o caminho absoluto do arquivo. No caso de arquivos nativos da Microsoft pode-se apenas escrever o nome do mesmo.

        ---
        ### Ex:
            - wg.proc_exec("C:\\Program Files\\Internet Explorer\\iexplore.exe")
            - wg.proc_exec("mspaint")

        ---
        #### Parâmetros:
            - cmd: Comando CMD a ser executado
        ---
        #### Retorna:
        
            -> "ok"
        """
        cmd = urllib.parse.quote(cmd, "")
        return self.__requestget__(f"/ipc/wingui/proc/exec?cmd={cmd}")

    def proc_kill(self, pid: int):

        """
        ## Process kill
        Finaliza o processo com PID informado como parâmetro.

        ---
        #### Parâmetros:
            - pid: Identificador do processo que se deseja finalizar.
        ---
        #### Retorna:

            - Sucesso: "Êxito, o processo com PID {num_do_pid} foi finalizado.".
            - Falha: Erro.
        """
        return self.__requestget__(f"/ipc/wingui/proc/kill?cmd={pid}")
    
    def proc_all(self):

        """
        ## Process All
        Lista todos os processos em execução na maquina robo.

        ---
        #### Parâmetros:

            ---

        ---
        #### Retorna:
            Um array de dicionários python. Cada um deles tem o formato:
                {
                    "Pid": xxx
                    "Name":"str"
                }
            Onde Pid é um inteiro representando o PID do processo, e Name é o nome do processo.
        """
        return self.__requestget__("/ipc/wingui/proc/all")

    def proc_name(self, pid: int):
        """
        ## Process Name
        Obtem o nome do processo através de seu PID.

        ---
        #### Parâmetros:

            - pid: Número do identificador de processo que se quer saber o nome.

        ---
        #### Retorna:
            -> Nome do processo correspondente ao PID.
        """
        return self.__requestget__(f"/ipc/wingui/proc/name?pid={pid}")

    def proc_path(self, pid: int):
        """
        ## Process Path
        Retorna o caminho do arquivo que corresponde ao PID informado.

        ---
        #### Parâmetros:
            - pid: Identificador do processo ativo que se quer saber o caminho.

        ---
        #### Retorna:
            -> Caminho do arquivo correspondente ao PID.
        """
        return self.__requestget__(f"/ipc/wingui/proc/path?pid={pid}")

    def proc_pids(self):
        """
        ## Process PIDS
        Busca todos os processos ativos na máquina e retorna o PID deles.

        ---
        #### Parâmetros:
            
            ---

        ---
        #### Retorna:
            -> Array de inteiros contendo todos os PIDS ativos da máquina.
        """
        return self.__requestget__("/ipc/wingui/proc/pids")       

# Funções Window
    def window_activehwnd(self) -> int:
        """
        ## Window Active HWND
        Procura o HWND da janela ativa entre as informações de processos ativos do Windows.

        ---
        #### info:
        HWND é um indicador unico de um componente ajanelado no windows.

        ---
        #### Parâmetros:

            ---

        ---
        #### Retorna:

            -> HWND da janela ativa.
        """
        return self.__requestget__("/ipc/wingui/window/activehwnd")

    def window_activetitle(self) -> str:
        """
        ## Window Active Title
        Procura o título da janela ativa entre as informações de processos ativos do Windows.

        ---
        #### Parâmetros:

            ---

        ---
        #### Retorna:

            -> Título da janela ativa
        """
        return self.__requestget__("/ipc/wingui/window/activetitle")

    def window_list(self, s: str = "") -> List[dict]:

        """
        ## Window List
        Lista as janelas disponiveis, fitrando pelo nome passado em "s", caso ele seja fornecido.

        ---
        #### info:
        HWND é um indicador unico de um componente ajanelado no windows.

        ---
        #### Parâmetros:
            - s: Substring do nome das janelas que se quer buscar.
        ---
        #### Retorna:
            - "s" recebe um valor: Lista de dicinários contendo todos os processos que contenham a substring de "s".
            - "s" não recebe um valor: Lista de dicionários contendo todos os processos ativos da máquina.
            ##### Formato de saída:
                {

                    "Hwnd": 0,

                    "Pid": 0,  

                    "Title": "str"  
                }

                Onde Hwnd é o identificador da janela, pid é o numero do processo, e Title é o titulo da janela
        """

        s = urllib.parse.quote(s, "")
        return self.__requestget__(f"/ipc/wingui/window/list?s={s}")

    def window_hwnd(self, s: str) -> List[int]:
        """
        ## Window HWND
        Faz uma busca nos processos ativos do windows, identifica os que possuem a susbtring informada em "s" e retorna o HWND de todos eles.

        ---
        #### info:
            HWND é um indicador unico de um componente ajanelado no windows.

        ---
        #### Parâmetros:
            - s: Substring contida na janela para busca. Este parâmetro é OBRIGATÓRIO.
        ---
        #### Retorna:
            -> Lista de todos os HWNDs cujo nome da janela possua a substring "s".
        """
        s = urllib.parse.quote(s, "")
        return self.__requestget__(f"/ipc/wingui/window/hwnd?s={s}")

    def window_activate(self, hwnd: int):
        """
        ## Window Activate
        Ativa uma janela de acordo com seu hwnd.

        ---
        #### info:
            HWND é um indicador unico de um componente ajanelado no windows.

        ---
        #### Parâmetros:
            - hwnd: Número HWND da janela que se deseja ativar.
        ---
        #### Retorna:

            ---
        """
        return self.__requestget__(f"/ipc/wingui/window/activate?Hwnd={hwnd}")

    def window_close(self, hwnd: int):
        """
        ## Window Close
        Fecha janela de acordo com o hwnd.

        ---
        #### info:
            HWND é um indicador unico de um componente ajanelado no windows.

        ---
        #### Parâmetros:
            - hwnd: Número HWND da janela que se quer fechar.
        ---
        #### Retorna:
            
            ---
        """
        return self.__requestget__(f"/ipc/wingui/window/close?Hwnd={hwnd}")

    def window_max(self, hwnd: int):
        """
        ## Window Maximize
        Maximiza janela de acordo com o hwnd.

        ---
        #### info:
        HWND é um indicador unico de um componente ajanelado no windows.

        ---
        #### Parâmetros:
            - hwnd: Número HWND da janela que se quer maximizar.
        ---
        #### Retorna:

            ---
        """
        return self.__requestget__(f"/ipc/wingui/window/max?Hwnd={hwnd}")

    def window_min(self, hwnd: int):
        """
        ## Window Minimize
        Minimiza janela de acordo com o hwnd.

        ---
        #### info:
        HWND é um indicador unico de um componente ajanelado no windows.

        ---
        #### Parâmetros:
            - hwnd: Número HWND da janela que se quer minimizar.
        ---
        #### Retorna:

            ---
        """
        return self.__requestget__(f"/ipc/wingui/window/min?Hwnd={hwnd}")

    def window_waitactivetitle(self, t: str, m: int = 30):
        """
        ## Window Wait for Active Title
        Aguarda um determinado tempo até que apareça uma janela ativa cujo título contenha a substring "t".

        ---
        #### Parâmetros:
            - t: Substring que deve estar contida no título da janela ativa.
            - m: Tempo limite até a janela aparecer.

        ---
        #### Retorna:
            - Dicionário contendo informações sobre a janela ativa com a substring encontrada.
            ##### Formato de saída:
                {

                    'Title': 'xxxxxxxx',

                    'Hwnd': xxxx
                }
        """
        t = urllib.parse.quote(t, "")
        return self.__requestget__(f"/ipc/wingui/window/waitactivetitle?t={t}&m={m}")

# Funções Display
    def display_res (self):
        """
        ## Display Resolution
        Faz uma busca nas configurações de sistema do windows para encontrar informações sobre o mesmo. Entre as informações encontradas, está a resolução do monitor ativo.

        ---
        #### Parâmetros:
            ---

        ---
        #### Retorna:
            - Dicionário contendo informações do sistema.
            ##### Formato de saída:
                {

                    "nome_da_propriedade": valor_da_propriedade

                    .

                    .

                    .

                    "nome_da_propriedade": valor_da_propriedade
                }
                
                As informações de comprimento e altura da tela podem ser encontradas através das chaves "DmPelsWidth" e "DmPelsHeight" respectivamente.
        """
        return self.__requestget__("/ipc/wingui/display/res")

    def display_setres (self, DEVMODE: dict):
        """
        ## Display Set Resolution
        Acessa as configurações do sistema e faz alterações na resolução da tela de acordo com os valores informados para a mesma.

        ---
        #### Parâmetros:
            - DEVMODE: Estrutura de dados com informações do sistema. Para mais informações, acesse: https://docs.microsoft.com/en-us/windows/win32/api/wingdi/ns-wingdi-devmodea
        
        ---
        #### Retorno:
            -> "ok"
        """
        return self.__requestpost__("/ipc/wingui/display/setres", DEVMODE)