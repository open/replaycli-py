from time import sleep
from behave import *
from cli import Wingui

# =========================== Clip_read & Clip_write ===========================
@given(u'a winGUI client')
def step_impl(context):
    context.client = Wingui ()

@when(u'the clip_write method is called to store the text "There is a light that never goes out"')
def step_impl(context):
    context.client.clip_write("There is a light that never goes out")

@then(u'when the clip_read method is called, it must return the same text')
def step_impl(context):
    assert context.client.clip_read() == "There is a light that never goes out", "Something went wrong with the clipboard."



# =========================== Screen_scale & Screen_size ===========================
@when(u'the screen_scale method is called')
def step_impl(context):
    context.scale = context.client.screen_scale()

@when(u'the screen_size method is also called')
def step_impl(context):
    context.size = context.client.screen_size()

@then(u'the returns of both must be equal')
def step_impl(context):
    assert context.scale == context.size, "Something went wrong while getting the screen dimensions."

# Please note that most of the other screen methods use image recognition and, because of that, it is not possible to test them on every computer, since there is no way to know wich images are being displayed on the screen.
    # Also, the other methods wich do not use image recognition are simply too difficult to test.



# =========================== Proc_exec & Proc_all ===========================
@when(u'the proc_exec method is called with "mspaint"')
def step_impl(context):
    context.client.proc_exec("mspaint")

@when(u'the proc_all method is called')
def step_impl(context):
    context.procs = context.client.proc_all()

@then(u'there must be a process with the name "mspaint.exe" inside the list')
def step_impl(context):
    for proc in context.procs:
        if "mspaint.exe" in proc["Name"]:
            context.client.proc_kill(proc["Pid"])
            assert True
            return
    
    assert False, "Something went wrong executing Microsoft Paint."



# =========================== Proc_name ===========================
@given(u'the initialization of Microsoft Paint')
def step_impl(context):
    context.client.proc_exec("mspaint")

@given(u'the "Name" and "Pid" infos about this process')
def step_impl(context):
    context.procs = context.client.proc_all()

    for proc in context.procs:
        if "mspaint.exe" in proc["Name"]:
            context.proc_name = proc["Name"]
            context.proc_pid = proc["Pid"]

@when(u'the proc_name method is called with the "Pid" collected')
def step_impl(context):
    context.name_return = context.client.proc_name(context.proc_pid)

@then(u'the return must be te same as the "Name" collected')
def step_impl(context):
    assert context.name_return == context.proc_name, "Something went wrong with some of the proc methods."

    context.client.proc_kill(context.proc_pid)



# =========================== Proc_pids ===========================
@when(u'the proc_pids is called')
def step_impl(context):
    context.all_pids = context.client.proc_pids()

@then(u'the "Pid" info collected must be inside the array returned')
def step_impl(context):
    assert context.proc_pid in context.all_pids, "Something went wrong catching all of the pids."
    
    context.client.proc_kill(context.proc_pid)



# =========================== Window_list and Window_hwnd ===========================
@when(u'the window_hwnd method is called with the parameter "mspaint.exe"')
def step_impl(context):
    sleep(0.5)
    context.paint_hwnd = context.client.window_hwnd("mspaint.exe")

@when(u'the window_list method is called')
def step_impl(context):
    context.windows = context.client.window_list()

@then(u'the return of window_hwnd must be the value of one of the "Hwnd" keys returned in window_list')
def step_impl(context):
    for window in context.windows:
        if context.paint_hwnd[0] == window["Hwnd"]:
            assert True
            sleep(0.5)
            context.client.proc_kill(context.proc_pid)

            return

    assert False, "Something went wrong catching the Paint window."



# # =================== Window_activate, Window_activetitle and Window_activehwnd ===================
# @given(u'the window hwnd obtained from the "Name" info')
# def step_impl(context):
#     context.paint_hwnd = context.client.window_hwnd(context.proc_name)

# @when(u'the window_activate method is called with the Paint Hwnd obtained')
# def step_impl(context):
#     sleep(0.5)
#     context.client.window_activate (context.paint_hwnd[0])
#     sleep(0.5)

# @when(u'the methods window_activetitle and window_activehwnd are called')
# def step_impl(context):
#     context.paint_title = context.client.window_activetitle()
#     context.paint_win_hwnd = context.client.window_activehwnd()

#     context.client.window_close(context.paint_hwnd[0])

# @then(u'the returned value from window_activetitle must contain the substring "Paint"')
# def step_impl(context):
#     assert "Paint" in context.paint_title, f"The active title obtained is not from Paint.\nActive Title: {context.paint_title}"

#     for window in context.client.window_list():
#         if context.paint_hwnd == window["Hwnd"]:
#             print(f"""

#             ---------------------------
#                 Hwnd associated title: {window["Title"]}
#             ---------------------------

#             """)

# @then(u'the returned value from window_activehwnd must be equal to the hwnd previously obtained')
# def step_impl(context):
#     assert context.paint_win_hwnd == context.paint_hwnd, "The Hwnd obtained is not from Paint"



# ======================================== Display methods ========================================
@given(u'the current resolution of the display')
def step_impl(context):
    context.sys_info = context.client.display_res()

    context.prev_width = context.sys_info["DmPelsWidth"]
    context.prev_height = context.sys_info["DmPelsHeight"]

@when(u'the display_setres method is called to set the display resolution in 800x600')
def step_impl(context):
    context.new_sys_info = context.sys_info
    context.new_sys_info["DmPelsWidth"] = 800
    context.new_sys_info["DmPelsHeight"] = 600

    try:
        context.client.display_setres(context.new_sys_info)
        sleep(5)
    except:
        raise "Your system configuration does not support 800x600 resolution"

@then(u'the new display resolution must be 800x600')
def step_impl(context):
    context.new_sys_info = context.client.display_res()

    assert context.new_sys_info["DmPelsWidth"] == 800 and \
           context.new_sys_info["DmPelsHeight"] == 600, \
               \
           "Something went wrong setting your system resolution to 800x600.\n\nCheck if this resolution is available in your configurations, if so, please disregard this message."

@then(u'the methods must be able to set your resolution to the previous one')
def step_impl(context):
    context.new_sys_info["DmPelsWidth"] = context.prev_width
    context.new_sys_info["DmPelsHeight"] = context.prev_height

    try:
        context.client.display_setres(context.new_sys_info)
        sleep(5)
    except:
        raise "Error!"

    context.curr_sys_info = context.client.display_res()

    assert context.curr_sys_info["DmPelsWidth"] == context.prev_width and \
           context.curr_sys_info["DmPelsHeight"] == context.prev_height, \
               \
           "Sorry for the inconvenience, but something went wrong resetting your resolution to the previous one.\n\nPlease go to your system configurations to do this step manually."