Feature: The winGUI API client

    Scenario: Testing the clip_read and clip_write methods
        Given a winGUI client
        When the clip_write method is called to store the text "There is a light that never goes out"
        Then when the clip_read method is called, it must return the same text
    

    Scenario: Testing the screen_scale and screen_size methods
        Given a winGUI client
        When the screen_scale method is called
        And the screen_size method is also called
        Then the returns of both must be equal
    
    # Please note that most of the other screen methods use image recognition and, because of that, it is not possible to test them on every computer, since there is no way to know wich images are being displayed on the screen.
    # Also, the other methods wich do not use image recognition are simply too difficult to test.


    Scenario: Testing proc_exec and proc_all methods
        Given a winGUI client
        When the proc_exec method is called with "mspaint"
        And the proc_all method is called
        Then there must be a process with the name "mspaint.exe" inside the list


    Scenario: Testing proc_name method
        Given a winGUI client
        And the initialization of Microsoft Paint
        And the "Name" and "Pid" infos about this process
        When the proc_name method is called with the "Pid" collected
        Then the return must be te same as the "Name" collected


    Scenario: Testing proc_pids method
        Given a winGUI client
        And the initialization of Microsoft Paint
        And the "Name" and "Pid" infos about this process
        When the proc_pids is called
        Then the "Pid" info collected must be inside the array returned
    

    Scenario: Testing the window_list and window_hwnd methods
        Given a winGUI client
        And the initialization of Microsoft Paint
        And the "Name" and "Pid" infos about this process

        When the window_hwnd method is called with the parameter "mspaint.exe"
        And the window_list method is called

        Then the return of window_hwnd must be the value of one of the "Hwnd" keys returned in window_list
    

    # Scenario: Testing window_activate, window_activetitle and window_activehwnd
    #     Given a winGUI client
    #     And the initialization of Microsoft Paint
    #     And the "Name" and "Pid" infos about this process
    #     And the window hwnd obtained from the "Name" info

    #     When the window_activate method is called with the Paint Hwnd obtained
    #     And the methods window_activetitle and window_activehwnd are called

    #     Then the returned value from window_activetitle must contain the substring "Paint"
    #     And the returned value from window_activehwnd must be equal to the hwnd previously obtained


    Scenario: Testing the display methods
        Given a winGUI client
        And the current resolution of the display

        When the display_setres method is called to set the display resolution in 800x600

        Then the new display resolution must be 800x600
        And the methods must be able to set your resolution to the previous one