from behave import *
from cli import Chrome

# ========================== Chrome.start () ==========================
@given ("the Chrome client")
def step_impl (context):
    context.client = Chrome()

@when ("the start method is called")
def step_impl (context):
    context.start_return = context.client.start()

@then ("Google Chrome must open")
def step_impl (context):
    assert context.start_return == "", "Google Chrome did not opened as expected."
    context.client.stop()


# ========================== Chrome.start_headless () ==========================
@when ("the start_headless method is called")
def step_impl (context):
    context.start_headless_return = context.client.start_headless()

@then ("Google Chrome must open headless")
def step_impl (context):
    assert context.start_headless_return == "", "Google Chrome did not opened as expected."
    context.client.stop()


# ========================== Chrome.stop () ==========================
@given ("a Chrome instance")
def step_impl (context):
    context.start_return = context.client.start()

@when ("the stop method is called upon it")
def step_impl (context):
    context.stop_return = context.client.stop()

@then ("this Google Chrome instance must close")
def step_impl (context):
    assert context.stop_return == "", "Google Chrome did not closed as expected."


# ========================== Chrome.new () ==========================
@when ("the new method is called")
def step_impl(context):
    context.tab_id = context.client.new("https://www.youtube.com")

@then ("a new Google Chrome tab with the url given must open in the instance")
def step_impl(context):
    assert len(context.tab_id) == 32, "The new Google Chrome tab did not opened correctly."
    context.client.stop()


# ========================== Chrome.close () ==========================
@given ("the id of a new Google Chrome tab opened")
def step_impl(context):
    context.tab_id1 = context.client.new("https://www.gmail.com")
    context.tab_id2 = context.client.new("https://www.youtube.com")

@when ("the close method is called upon the tab id")
def step_impl(context):
    context.close_return = context.client.close(context.tab_id1)

@then ("the tab must close")
def step_impl(context):
    assert context.close_return == "", "The tab did not closed correctly."
    context.client.stop()


# ========================== Chrome.eval () ==========================
@when ("the eval method is called upon the tab id with a arithmetic command")
def step_impl(context):
    context.eval_value_arit = context.client.eval(context.tab_id1, "2+2")

@then ("this command must give back the result as a value")
def step_impl(context):
    assert context.eval_value_arit == 4, "The command did not worked as expected."
    context.client.stop()
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
@given ("the id of a new Google Chrome tab opened in google.com")
def setp_impl(context):
    context.tab_id = context.client.new("https://www.google.com/")

@when ("the eval method is called upon the tab id with a JS command")
def step_impl(context):
    _ = context.client.wait(context.tab_id, "document.getElementById('SIvCob') != undefined", 5)
    context.eval_value_js = context.client.eval(context.tab_id, "document.getElementById('SIvCob').textContent")

@then ("this command must give back the element requested as a value")
def step_impl(context):
    assert context.eval_value_js == 'Disponibilizado pelo Google em:  English  ', "The command did not worked as expected."
    context.client.stop()

# ========================== Chrome.wait () ==========================
@when("the wait method is called upon the tab id with a JS command")
def step_impl(context):
    context.wait_return = context.client.wait(context.tab_id, "document.getElementById('SIvCob') != undefined", 5)

@then("this method must return the string 'ok'")
def step_impl(context):
    assert context.wait_return == "ok", "The command did not worked as expected."
    context.client.stop()

# ========================== Chrome.send () ==========================
"""
This method cannot be tested, given it's return cannot be predicted.
Therefore, there will be no tests for this method, at least for now.
"""

# ========================== Chrome.open_tabs () ==========================
@given("at least one Google Chrome tab is opened")
def step_impl(context):
    context.tab_id1 = context.client.new("https://www.google.com/")
    context.tab_id2 = context.client.new("https://www.youtube.com")

@when("the open_tabs method is called")
def step_impl(context):
    context.open_tabs_return = context.client.open_tabs()

@then("it must return a list of dictionaries containing the tabs data")
def step_impl(context):
    assert context.open_tabs_return[1]["id"] == context.tab_id1, f"""
    ----------
    Google id: {context.tab_id1}
    First obj id: {context.open_tabs_return[1]['id']}
    Match: {context.open_tabs_return[1]['id'] == context.tab_id1}
    ----------
    """
    context.client.stop()

# ======================== Chrome.find_tab_by_url() ========================
    # This method is currently broken.
    # Therefore, there will be no testes for it, at least for now.

# ======================== Chrome.find_tab_by_title() ========================
@when("the find_tab_by_title method is called")
def step_impl(context):
    context.ftbt_return = context.client.find_tab_by_title("Google")

@then("the id that this method returned must match the id of the tab searched")
def step_impl(context):
    assert context.ftbt_return == context.tab_id1, f"""
    First tab id: {context.tab_id1}
    Returned id: {context.ftbt_return}
    Match: {context.ftbt_return == context.tab_id1}
    """
    context.client.stop()