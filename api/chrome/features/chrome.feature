Feature: The Chrome API client
# ======================== Chrome.start() ========================
    Scenario: Using the Chrome.start method
        Given the Chrome client
        When the start method is called
        Then Google Chrome must open

# ======================== Chrome.start_headless() ========================
    Scenario: Using the Chrome.start_headless method
        Given the Chrome client
        When the start_headless method is called
        Then Google Chrome must open headless

# ======================== Chrome.stop() ========================
    Scenario: Using the Chrome.stop method
        Given the Chrome client
        And a Chrome instance
        When the stop method is called upon it
        Then this Google Chrome instance must close

# ======================== Chrome.new() ========================
    Scenario: Using the Chrome.new method
        Given the Chrome client
        And a Chrome instance
        When the new method is called
        Then a new Google Chrome tab with the url given must open in the instance

# ======================== Chrome.close() ========================
    Scenario: Using the Chrome.close method
        Given the Chrome client
        And a Chrome instance
        And the id of a new Google Chrome tab opened
        When the close method is called upon the tab id
        Then the tab must close

# ======================== Chrome.eval() ========================
    Scenario: Using the Chrome.eval method for arithmetics
        Given the Chrome client
        And a Chrome instance
        And the id of a new Google Chrome tab opened
        When the eval method is called upon the tab id with a arithmetic command
        Then this command must give back the result as a value

    Scenario: Using the Chrome.eval method for JavaScript commands
        Given the Chrome client
        And a Chrome instance
        And the id of a new Google Chrome tab opened in google.com
        When the eval method is called upon the tab id with a JS command
        Then this command must give back the element requested as a value

# ======================== Chrome.wait() ========================
    Scenario: Using the Chrome.wait method
        Given the Chrome client
        And a Chrome instance
        And the id of a new Google Chrome tab opened in google.com
        When the wait method is called upon the tab id with a JS command
        Then this method must return the string 'ok'

# ======================== Chrome.send() ========================
    # This method cannot be tested, given it's return cannot be predicted.
    # Therefore, there will be no testes for this method, at least for now.

# ======================== Chrome.open_tabs() ========================
    Scenario: Using the Chrome.open_tabs method
        Given the Chrome client
        And a Chrome instance
        And at least one Google Chrome tab is opened
        When the open_tabs method is called
        Then it must return a list of dictionaries containing the tabs data

# ======================== Chrome.find_tab_by_url() ========================
    # This method is currently broken.
    # Therefore, there will be no testes for it, at least for now.

# ======================== Chrome.find_tab_by_title() ========================
    Scenario: Using the Chrome.find_tab_by_title method
        Given the Chrome client
        And a Chrome instance
        And at least one Google Chrome tab is opened
        When the find_tab_by_title method is called
        Then the id that this method returned must match the id of the tab searched