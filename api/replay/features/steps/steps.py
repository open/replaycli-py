from behave import *
from cli import Replay

from datetime import datetime

# ============================= ENVIRONMENT VARIABLES GETTERS =============================
@given(u'the Replay client')
def step_impl (context):
    context.client = Replay ()

@when(u'each one of the getters is called')
def step_impl(context):
    context.returns = {context.client.replay_env_queue_id(): "context.client.replay_env_queue_id()", context.client.replay_env_alias(): "context.client.replay_env_alias()", context.client.replay_env_feature_dir(): "context.client.replay_env_feature_dir()", context.client.replay_env_root(): "context.client.replay_env_root()", context.client.replay_env_addr(): "context.client.replay_env_addr()", context.client.replay_env_home_dir(): "context.client.replay_env_home_dir()", context.client.replay_env_repo(): "context.client.replay_env_repo()", context.client.replay_env_ver(): "context.client.replay_env_ver()", context.client.replay_env_repo_dir(): "context.client.replay_env_repo_dir()", context.client.replay_env_data_dir(): "context.client.replay_env_data_dir()", context.client.replay_env_instance_alias(): "context.client.replay_env_instance_alias()", context.client.replay_env_api_key(): "context.client.replay_env_api_key()"}

@then(u'all of the returns must be either of the type string or the type None')
def step_impl(context):
    for ret in context.returns.keys():
        assert ( type(ret) == type(None) or type(ret) == type("") ), f'Something went wrong with the {context.returns[ret]} variable getter.'



# ============================= LOG REGISTERS =============================
@given(u'a Log created')
def step_impl(context):
    context.log = context.client.new_log()

@when(u'the log method is called')
def step_impl(context):
    context.dt_log_end = datetime.now().isoformat() + "Z"
    context.log_return = context.client.log(context.log)

@then(u'this new log must be registered in the Replay Data Base')
def step_impl(context):
    assert (context.client.sql("select * from logs")["data"][-1]["dtlogend"] == context.dt_log_end and context.log_return == "ok")



# ============================= CONFIGURATION GETTERS =============================
    # Since this is a remote repository, it is impossible to test the config getters methods.
    # They have to be runned inside a local repository wich contains Replay, so, unfortunatelly, they will not be tested, at least not for now.



# ============================= OPEN APP =============================
     #It is also not possible to test the open_app method for the same reasons as above.
    


# ============================= GET ALL ENABLED MENUS =============================
@when(u'the menu_get_all_enabled method is called')
def step_impl(context):
    context.all_menus_return = context.client.menu_get_all_enabled()

@then(u'there must be a list as a return')
def step_impl(context):
    assert type(context.all_menus_return) == type([]), "Something went wrong getting the enabled menus."



    # Given the fact that the methods "service_stop_all" and "exit" will break the tests chain, they cannot be tested.



    # The same from above goes for the "exit" method.



    # The queue methods needs some parameters, wich cannot be given, since this test is not made to be launched through Replay and there is no way to know wich are the "job_id"s or "queue_id"s from the tester machine.



# ============================= QUEUE ABORT =============================
@when(u'the queue_abort method is called')
def step_impl(context):
    context.queue_abort_return = context.client.queue_abort()

@then(u'there must be None as return, since this test was not runned by Replay')
def step_impl(context):
    assert (context.queue_abort_return == None), 'Something went wrong with queue_abort.'
