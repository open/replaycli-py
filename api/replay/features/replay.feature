Feature: The Replay API client

    Scenario: Testing every environment variable getter
        Given the Replay client
        When each one of the getters is called
        Then all of the returns must be either of the type string or the type None


    Scenario: Testing the process of creating and registering a Log
        Given the Replay client
        And a Log created
        When the log method is called
        Then this new log must be registered in the Replay Data Base
    

    # Since this is a remote repository, it is impossible to test the config getters methods.
    # They have to be runned inside a local repository wich contains Replay, so, unfortunatelly, they will not be tested, at least not for now.


    #It is also not possible to test the open_app method for the same reasons as above.


    Scenario: Testing the menu_get_all_enabled method.
        Given the Replay client
        When the menu_get_all_enabled method is called
        Then there must be a list as a return
    

    # Given the fact that the methods "service_stop_all" and "exit" will break the tests chain, they cannot be tested.


    # The same from above goes for the "exit" method.


    # The queue methods needs some parameters, wich cannot be given, since this test is not made to be launched through Replay and there is no way to know wich are the "job_id"s or "queue_id"s from the tester machine.


    Scenario: Testing queue_abort method
        Given the Replay client
        When the queue_abort method is called
        Then there must be None as return, since this test was not runned by Replay