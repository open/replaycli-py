import os
import json
import requests

class AntiCaptcha:
    """
    ## AntiCaptcha Client
    ---
    Esta classe utiliza o serviço svc_anticaptcha para quebrar provas de robô presentes em sites.
    """

    ep: str = ""

    def __init__(self) -> None:
        self.ep = os.environ.get("REPLAY_ADDR")
    
        def __request_json_post__ (self, path: str, object: dict):
            """
            ## HTTP JSON POST
            ---
            Este método é responsável por realizar requisições HTTP do tipo POST para objetos JSON.

            Ele retorna o corpo de resposta da requisição, ou uma mensagem de erro, que indica qual foi a irregularidade ocorrida ao chamar a API.
            """

            url = self.ep + path
            print("Calling: " + url)
            
            apikey = os.environ.get('REPLAY_APIKEY')
            headers = {"X-API-KEY": apikey}
            res = requests.post(url, json = object, headers = headers, verify = False)

            if res.status_code >= 400:
                raise Exception(f"HTTP ERROR: {str(res.status_code)} - {res.text}")
            if res.headers.get("Content-Type") != None and res.headers.get("Content-Type").find("json") != -1:
                return json.loads(res.text)
            else:
                return res.text