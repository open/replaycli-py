from behave import *

from cli import AutoIt

# ================================== AutoIt Test ==================================
@given(u'the AutoIt client')
def step_impl(context):
    context.client = AutoIt()

@when(u'the test method is called')
def step_impl(context):
    context.test_return = context.client.test()

@then(u'the string "Autoit Svc Ok" must be returned')
def step_impl(context):
    assert context.test_return == "Autoit Svc Ok", "There is something wrong with your AutoIt."


# ================================== AutoIt Do ==================================
@when(u'the do method is called with "ClipPut ( "test" )"')
def step_impl(context):
    context.do_return = context.client.do('ClipPut ( "test" )')

@then(u'the string "test" must me recorded on ClipBoard')
def step_impl(context):
    print (f"""
    -----------------
    {context.client.do ( "ClipGet ( )" )}
    -----------------
    """)
    assert context.client.do( "ClipGet ()" ) == "", "Something went wrong with the ClipPut method."