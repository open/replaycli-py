Feature: The AutoIt API client
# ======================== AutoIt Test ========================
    Scenario: Testing the AutoIt client
        Given the AutoIt client
        When the test method is called
        Then the string "Autoit Svc Ok" must be returned

# ======================== AutoIt Test ========================
    Scenario: Using an AutoIt command through AutoIt client
        Given the AutoIt client
        When the do method is called with "ClipPut ( "test" )"
        Then the string "test" must me recorded on ClipBoard