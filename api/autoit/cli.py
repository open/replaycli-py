import os
import requests
import json

class AutoIt:
    """
    ## AutoIt Client
    ---
    Esta classe utiliza o serviço "svc_autoit" para realizar operações AutoIt no computador através do Python.

    Para que esta biblioteca funcione corretamente, é necessário que você tenha o AutoIt instalado na máquina, com o caminho do mesmo apontado pela variável de ambiente PATH.

    - Baixar AutoIt: https://www.autoitscript.com/site/autoit/downloads/
    """
    
    ep: str = ""

    def __init__ (self):
        self.ep = os.environ.get("REPLAY_ADDR")
    
    def __request_raw_post__ (self, path: str, data: str = ""):

        """
        ## HTTP RAW POST
        ---
        Este método é responsável por realizar requisições HTTP do tipo POST para objetos RAW.

        Ele retorna o corpo de resposta da requisição, ou uma mensagem de erro, que indica qual foi a irregularidade ocorrida ao chamar a API.
        """

        url = self.ep + path
        print("Calling: " + url)
        
        apikey = os.environ.get('REPLAY_APIKEY')
        headers = {"X-API-KEY": apikey}
        res = requests.post(url, data = data, headers = headers, verify = False)

        if res.status_code >= 400:
            raise Exception(f"HTTP ERROR: {str(res.status_code)} - {res.text}")
        if res.headers.get("Content-Type") != None and res.headers.get("Content-Type").find("json") != -1:
            return json.loads(res.text)
        else:
            return res.text
    
    def test (self):
        """
        ## AutoIt test
        Este método é utilizado para testar se o AutoIt está corretamente instalado na máquina do usuário, além de garantir que o serviço também está instalado e funcionando corretamente.
        
        ---
        #### Parâmetros:
            ---
        
        ---
        #### Retorna:
            -> "Autoit Svc Ok" caso tudo esteja funcionando corretamente.
        """

        return self.__request_raw_post__ ("/ipc/autoit/test")
    
    def do (self, command: str):
        """
        ## AutoIt Do
        Realiza algum comando Autoit passado como parâmetro.

        ---
        #### Parâmetros: 
            - command: Comando autoit que se deseja executar

        ---
        #### Retorna:
            -> O retorno do comando, se houver.
        """
    
        return self.__request_raw_post__ ("/ipc/autoit/do", command)
