from behave import *
from cli import *

# ============================= DATA_API DO =============================
@given(u'a data_API client')
def step_impl(context):
    context.client = DataAPI ("Insira aqui a chave de API. Você pode obtê-la pedindo ao fornecedor deste client.")

@when(u'a new table is created with the value "Bond" for the query "the_name_is"')
def step_impl(context):
    context.client.do ({
        "Col": "newTestTable",
        "Op": "C",
        "Data": {
            "the_name_is": "Bond",
            "James": "Bond"
        }
    })

@then(u'the value retrieved from the new table associated with the query "the_name_is" must be "Bond"')
def step_impl(context):
    context.retrieved_data = context.client.do({
                                "Col": "newTestTable",
                                "Op": "R"
                            })["data"][0]

    context.query_value = context.retrieved_data["the_name_is"]

    assert context.query_value == "Bond", "Something went wrong with the method Do"


# ============================= DATA_API REGISTRAR_EXEC =============================
@when(u'the registrar_exec method is called with True for check')
def step_impl(context):
    context.client.registrar_exec("anotherNewTestTable", True)

@when(u'the registrar_exec method is called with False for check')
def step_impl(context):
    context.client.registrar_exec("anotherNewTestTable", False)

@when(u'data is retrieved from the table')
def step_impl(context):
    today_date = datetime.now().isoformat()[:10]

    context.retrieved_data = context.client.do({
            "Col": "anotherNewTestTable",
            "Op": "R",
            "Q": f"@[?date=='{today_date}']"
        })["data"][0]

@then(u'there must be at least 1 register for "exec" and "err" today')
def step_impl(context):
    assert context.retrieved_data["exec"] > 0 and context.retrieved_data["err"] > 0, "Something went wrong with the method registrar_exec"