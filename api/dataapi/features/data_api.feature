Feature: The Data API Client
    Scenario: Testing the data_API.do() method
        Given a data_API client
        When a new table is created with the value "Bond" for the query "the_name_is"
        Then the value retrieved from the new table associated with the query "the_name_is" must be "Bond"
    

    Scenario: Testing the data_API.registrar_exec method
        Given a data_API client
        When the registrar_exec method is called with True for check
        And the registrar_exec method is called with False for check
        And data is retrieved from the table
        Then there must be at least 1 register for "exec" and "err" today