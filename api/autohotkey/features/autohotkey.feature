Feature: The AutoHotKey API client
# ======================== AutoHotKey Test ========================
    Scenario: Testing the AutoHotKey client
        Given the AutoHotKey client
        When the test method is called
        Then the string "AHK Svc Ok" must be returned

# ======================== AutoHotKey Test ========================
    Scenario: Using an AutoHotKey command through AutoHotKey client
        Given the AutoHotKey client
        When the do method is called
        Then if there is a return in AHK, it must appear