from behave import *

from cli import AutoHotKey

# ================================== AutoHotKey Test ==================================
@given(u'the AutoHotKey client')
def step_impl(context):
    context.client = AutoHotKey ()

@when(u'the test method is called')
def step_impl(context):
    context.test_return = context.client.test ()

@then(u'the string "AHK Svc Ok" must be returned')
def step_impl(context):
    assert context.test_return == "AHK Svc Ok", "There is something wrong with te AHK or it's service."


# ================================== AutoHotKey Do ==================================
@when(u'the do method is called')
def step_impl(context):
    context.do_return = context.client.do ('clipboard := "Riptide"')

@then(u'if there is a return in AHK, it must appear')
def step_impl(context):
    assert context.do_return == "", "Something went wrong with the command given."