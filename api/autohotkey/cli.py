import os
import requests
import json

class AutoHotKey:
    """
    ## AutoHotKey
    ---
    Esta classe utiliza o serviço svc_autohotkey para realizar operações AutoHotKey na máquina.

    Para que o client funcione corretamente, é necessário que o autoHotKey esteja instalado na máquina e o caminho de seu executável gravado na variável de ambiente PATH.

    - Baixar AutoHotKey: https://www.autohotkey.com/
    """

    ep: str = ""

    def __init__ (self):
        self.ep = os.environ.get("REPLAY_ADDR")
    
    def __request_raw_post__ (self, path: str, data: str = ""):

        """
        ## HTTP RAW POST
        ---
        Este método é responsável por realizar requisições HTTP do tipo POST para objetos RAW.

        Ele retorna o corpo de resposta da requisição, ou uma mensagem de erro, que indica qual foi a irregularidade ocorrida ao chamar a API.
        """

        url = self.ep + path
        print("Calling: " + url)
        
        apikey = os.environ.get('REPLAY_APIKEY')
        headers = {"X-API-KEY": apikey}
        res = requests.post(url, data = data, headers = headers, verify = False)

        if res.status_code >= 400:
            raise Exception(f"HTTP ERROR: {str(res.status_code)} - {res.text}")
        if res.headers.get("Content-Type") != None and res.headers.get("Content-Type").find("json") != -1:
            return json.loads(res.text)
        else:
            return res.text

    def test (self):
        """
        ## AutoHotKey Test
        Este método é utilizado para testar se o AutoHotKey está corretamente instalado na máquina do usuário, além de garantir que o serviço do mesmo também está instalado e funcionando corretamente.
        
        ---
        #### Parâmetros:
            ---
        
        ---
        #### Retorna:
            -> "AHK Svc Ok" caso tudo esteja funcionando corretamente.
        """
        return self.__request_raw_post__ ("/ipc/ahk/test")
    
    def do (self, command: str):
        """
        ## AutoHotKey Do
        Realiza algum comando AutoHotKey passado como parâmetro.

        ---
        #### Parâmetros: 
            - command: Comando AutoHotKey que se deseja executar

        ---
        #### Retorna:
            -> O retorno do comando, se houver.
        """

        return self.__request_raw_post__ ("/ipc/ahk/do", command)
